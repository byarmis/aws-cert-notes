# AWS Object Storage and CDN - S3, Glacier, and CloudFront

## S3 101
*Simple Storage Service (S3) provides deveolopers and IT teams with secure, durablabe, scalabe object storage*

* One of the first services on AWS
* Object-based storage
    * Store pics, files, videos, etc.
    * Not where you install something or set up a database
    * Files can be up to 5TB
    * Unlimited storage, but pay by the gigabyte
* Files are stored in buckets
    * Effectively just a folder in AWS
    * Can have folders in buckets
    * Bucket names must be globally unique
* Receive HTTP 200 if the upload was successful 
* S3 is effectively a key-value store
    * Key- the name of the object
    * Value- the data
    * Version ID- used for versioning
    * Metadata- data about the data
        * Tags, updated timestamps, etc.
    * Subresources
        * Access Control Lists
            * Fine-grained access controls at the file-level
        * Torrent
            * Used for torrenting files
            * Not an exam topic

### Data consistency model
* Read after write consistency for PUTs of new objects
    * If you write a new file then immediately read it, you'll get that data
* Eventual consistency for overwrite PUTs and DELETEs
    * Overwriting and deletion can take some time to propagate across Availability Zones
    * If you update or delete an existing file, you many get the new file or the old while the changes are propagating

* Built for 99.99% availability-- 99.9% availability guaranteed
* Guaranteed 11 9s durability of S3 information
    * If you upload X files, 99.99.. x X of those files will be kept
    * Effectively 100% durable
* Tiered storage available-- different access speeds and costs
* Lifecycle management-- move files to different storage tiers based off of access
* Versioning-- can have version control on a file
* Encryption-- files can be encrypted
* MFA Delete-- object deletion requires 2FA
* Access control-- data can be secured with access control lists and bucket policies
    * Bucket access as well as file access can be controlled

### S3 Storage Classes
#### S3 Standard
* 4 9s availabilty, 11 9s durability
* Stored accross mulitple devices in multiple facilities
* No standard retrieval fee

#### S3 Infrequently Accessed (S3 IA)
* Accessed less-frequently, but requires rapid access when needed
* Cheaper than S3 Standard, but charged a retrieval fee

#### S3 One Zone IA
* Cheaper than normal S3 IA
* Does not have the multiple availability zone data resilience
* Good way to store recreatable data

#### S3 Intelligent Tiering
* Uses machine learning to move objects to different storage tiers based on access to optimize cost

#### S3 Reduced Redundancy
* For frequently accessed data, but reproducible
* Cheaper than standard, but about as fast and no retrieval fee

#### Glacier
* Cheap, but for archival only
* Has expedited, standard, and bulk options
    * Expedited retrieval time: a few minutes 
    * Standard retrieval time: 3 - 5 hours
    * Bulk retrieval time: 5 - 12 hours

#### Glacier Deep Archive
* Cheapest
* Similar to Glacier, but has a 12 hour retrieval time

### Pricing and Charges
* Charged for storage (per gigabyte)
* Number of requests
* Storage management pricing
    * Can add tags to an object that's uploaded 
    * Charged for the metadata
* Data transfer
    * Moving data from one region to another (cross-region replication)
* Transfer acceleration
    * Upload data to an AWS edge location (CloudFront CDN location)
    * Data is then moved to an S3 location over Amazon's backbone network
    * There are many more CloudFront locations than S3 locations so those are probably closer to users than an S3 data center
    * Faster data upload

## Lab - Create an S3 Bucket

* S3 does not require region selection
* Buckets are managed at a global level
* Bucket names must be unique
    * Use a dns namespace
    * Buckets have a web address
* Buckets do have to have a region
* Can copy settings from existing bucket
* Buckets can have tags.
    * Tags have a key and a value
    * Objects within the bucket do not inherit the bucket's tags
* Manage public permissions
    * By default, all buckets are private
* Manage system permissions
    * By defualt, buckets can't be used for logging
* Uploading files
    * Can upload a file just from the web browser
    * Will get http 200 code on successful upload
* Each object has a link
    * By default, all buckets are private so all objects in them are private as well
    * Individual objects can be made public while the rest of the bucket remains private
* Permissions
    * Owner access- what the owner of the account can do
    * Access for other aws accounts- add accounts with different perms
    * Public access- what everyone can do
    * Permissions can be granted to
        * List and / or write objects
        * Read and / or write bucket permissions
    * Permissions can be granted via access control lists (ACLs) or Bucket Policies
        * Bucket policies can be generated
            * There are different policy types pre-generated
            * Can allow or deny different actions by aws principal name (username plus numbers)
                * \* is everyone
            * Can select actions that the policy governs
            * Need amazon resource name (arn)
                * arn:aws:s3:::<bucket_name>/<key_name>
* Can enable server-side encryption
    * Encrypted on aws disks
    * Can also be encrypted client-side
* Can add object-level metadata

## Lab - Version Control

* Stores all versions of an object 
    * Writes
    * Deletes
* Versioning is controlled in the Properties tab
    * Once enabled, versioning cannot be disabled, only suspended
* Each version of an object is stored as its own object
    * Frequently changing, large files will balloon the size of the bucket
* Can view different versions by clicking on the object's "Latest version" dropdown
    * To restore a file:
        * Can download older version and re-upload
        * Can also delete versions from the versioning dropdown
    * Objects aren't really hard-deleted
        * When deleted, a flag is added indicating that they were deleted
            * Delete marker can be removed, un-deleting the object
        * When versions are shown, can view deleted files
* Can configure a bucket to enable MFA deletes
    * Requires MFA to change the versioning state or permanently delete a version

## Lab - Cross Region Replication

* CRR needs versioning enabled in both buckets
* Can replicate only folders, not necessarily the whole bucket
* Can replicate to buckets in other aws accounts
* Can replicate to a different storage class
    * Standard -> standard IA, for instance, if used as a backup
* After CRR is enabled, existing objects are not copied over
    * Only new changes
* Deletions are replicated over as well
    * Deleting a delete marker (undeleting the object) is not replicated over
    * Reverting to a previous version is also not replicated over
* Not able to replicate to multiple buckets or daisy chain buckets
    * Can't do `A -> B -> C`
    * Can't do `A -> B` and `A -> C`

## Lab - Lifecycle Management & Glacier

* Glacier isn't available in every region
* Versioning isn't required
* Accessed via the s3 bucket management tab
* Lifecycle rules can only be applied to specific folders or tags if desired
* UI walks you through creating S3 tier transitions
* Can permanently delete something too if you want

## CloudFront CDN Overview

*A Content Delivery Network (CDN) is a network of servers that delivers content to a user based on the location of that user from Edge Locations.*

### Vocab
#### Edge Location
Where content will be cached.  This is separate from an AWS Region or Availability Zone.  There are significantly more Edge Locations than AWS AZs.

Edge locations are not just read-only.  Objects can be uploaded to an Edge Location.

#### Origin
The source of the files that the CDN will distribute.  Can be an S3 bucket, EC2 instance, Elastic Load Balancer (ELB), or Route53.

#### Distribution
The name given to the CDN, consisting of a collection of Edge Locations.  You can have two different types of distributions:

* Web distribution-- typically used for websites
* RTMP-- used for media streaming

### How It Works

Users requests for an object go to the Edge Location closest to them.  If the object is cached, the Edge Location returns it.  If not, the object is sent from the origin to the Edge Location and returned to the user.  Future requests during the Time To Live (TTL), while the object is cached at the Edge Location, recieve the cached object.

The origin server need not be on AWS.

## Lab - Create a CloudFront CDN
 
| Option | Notes |
|--------|-------|
| Origin domain name | <ul><li> Where is our origin coming from</li><li>Can select buckets, elbs, ec2 instances, etc.</li> |
| Origin path | <ul><li>A folder within an s3 bucket</li><ul> |
| Origin id | <ul><li>The name of the origin</li><li>Naming it for yourself, doesn't have to be the bucket name</li></ul> |
| Restrict bucket access | <ul><li>Can restrict bucket access so all requests to a bucket have to go through cloudfront</li></ul> |
| Origin access identity | <ul><li>A way of restricting access to buckets</li></ul> |
| Grant read permissions on bucket | <ul><li>Distribution needs read access on the bucket.  Should the creation step ensure that automatically or do you want to do it yourself</li></ul> |
| Path pattern | <ul><li>When you have multiple origins, can route traffic based on regexes</li></ul> |
| Viewer protocol policy | <ul><li>HTTP and HTTPS</li><li>Redirect HTTP to HTTPS</li><li>HTTPS only</li></ul> |
| Allowed http methods | <ul><li>GET, HEAD</li><li>GET, HEAD, OPTIONS</li><li>GET, HEAD, OPTIONS, PUT, POST, PATCH, DELETE</li><li>File is first stored on edge location and then origin is updated</li></ul> |
| Cached HTTP methods | <ul><li>GET, HEAD cached by default</li><li>Can cache OPTIONS</li></ul> |
| TTLs | <ul><li>Units are seconds</li><li>Minimum / maximum / default per object</li></ul> |
| Restrict viewer access | <ul><li>Authenticate users when they try to access an object</li></ul> |
| Alternate domain names | <ul><li>Default URLs are really ugly</li><li>Can use nicer URLs but you need a custom SSL certificate</li></ul> |
| Logging | <ul><li>Can log to a bucket</li><li>Can log to a folder in a bucket</li></ul> |

* Geo-Restrictions
    * Can restrict content based on geographical location
    * Can either whitelist or blacklist countries, but not both

## Security & Encryption

* By default, all new buckets are private
* You can set up access control to buckets by using
    * Bucket policies
        * Bucket-wide
    * Access Control Lists
        * Can be object-specific
* Can log all access requests, to a different bucket if you want

### Types of Encryption
#### In Transit
* Sending or recieving data to or from the bucket
    * SSL / TLS

#### At Rest
* Server-side encryption
    * S3 Managed Keys (SSE-S3)
        * Each object encrypted with a unique key
        * Click on object, hit encrypt
    * AWS Key Management Service (SSE-KMS)
        * Can have different permissions for envelope key
        * Provides audit trail of what keys were used where; who decrypted what, when
    * Server-Side Encryption with Customer-provided keys (SSE-C)
        * You manage the encryption keys
* Client-side encryption
    * You encrypt the data and then upload it

## Storage Gateway
*AWS Storage Gateway connects on-premises sofware with cloud-based storage.  This allows you to store data in AWS for scalable storage.*

Installed into a hypervisor running in your datacenter.  This then asynchronously replicates data to AWS (S3).  This hypervisor is available as a VM image download.

### File Gateway
* Uses Network File System (NFS)
* Designed for storing flat-files
* Ownership, permissions, and timestamps are stored as user-metadata
* Once uploaded, objects can be managed like any other S3 object (versioning, lifecycle management, cross-region replication)

### Volume Gateway
* Uses iSCSI
* For block-based storage 
    * Operating systems, databases, applications
* Effectively a virtual hard-disk
* Can be asynchronously backed up as a point-in-time snapshot
* Snapshots are incremental backups 

* Stored Volumes-- entire copy of data set is stored locally
    * 1 GB - 16 TB
* Cached Volumes-- store only most-recently-accessed data on-prem
    * 1 GB - 32 TB

### Tape Gateway
* Uses Virtual Tape Libraries (VTL)
* Archiving solution
* Allows you to create virtual tapes, send them to S3, and use lifecycle management on the files
* Works with existing tape backup applications

## Snowball
* For moving large amounts of data into or out of S3 once
* Used to be called Import Export, but you had to send in your own disks
    * Became a logistical nightmare
* Disks are sent to you ->  Load up disks with data -> Send the disks back to AWS -> Data is ingested into S3

### Snowball
* Larger than a briefcase-- large computer case, roughly
* 80TB

### Snowball Edge
* Physically similar to Snowball
* Comes with 100TB of storage
* Comes with some compute capacity
    * Can run lambda functions on them
* Mini, portable AWS datacenter

### Snowmobile
* Shipping container-sized
* For transfering up to 100PB of data per Snowmobile

## Lab - Snowball

Steps:
1. Create job
    * Plan your job-- what's happening (import / export)
    * Give shipping details-- where it's going
    * Give job details--what bucket it's going to
    * Set security-- how do you want it encrypted
    * Set notifications--sends emails with step notifications
1. Prepare appliance
1. Prepare shipment
1. Ship to you
1. Deliver to you
1. Ship to AWS
1. At AWS
1. Import data
1. Done

* Need to download and install Snowball command-line tools
* Can use RJ45, SFP Copper, or SFP Optical
* Need to get client-specific unlock code and download a manifest file

## S3 Transfer Acceleration

*Utilises the CloudFront Edge Network to accelerate uploads to S3.  Instead of uploading directly to an S3 bucket, Transfer Acceleration allows uploads to an edge location which then transfers the file to S3.*

* Uploads require a different URL compared to normal S3 uploads
    * `http(s)://<bucket name>.s3-accelerate.amazonaws.com`
* Enabled under bucket properties

## Lab - Create a Static Website Using S3

* No mainentnace or servers involved
    * Very fast way of getting a website up and running
    * Can handle a lot of traffic cheaply
* Enabled under bucket properties
* Static only
    * Can't have anything dynamic (PHP, ASP, etc.)
    * HTML and other static files (images, etc.)

1. Create HTML
1. Upload it to S3
1. Make bucket public
1. Visit!

* Will need to use Route53 if you want to set up DNS forwarding
    * mywebsite.com -> mywebsite.s3-website.us-east-2.amazonaws.com
    * In order for Route53 to work with S3, the domain name and the bucket name *must* be the same
* Create new bucket
* Enable static website hosting
    * URL is `http(s)://<bucket name>.s3-website.<region>.amazonaws.com`
* Can set:
    * Default file for index 
    * Default file for error
    * Redirection rules
* Need to have public read access on pages uploaded
    * Should probably set at the object level, not the bucket level

## Exam Tips

* Object-based storage for files
* Files can be from 0B to 5TB
* Unlimited storage
* Files are stored in buckets
* Buckets have a universal namespace
    * URL format `http(s)://s3-<region>.amazonaws.com/<bucket name>`
* Read after write consistency for PUTs of new objects
    * After an object is written, can immediately read it back
* Eventual consistency for overwrite PUTs and DELETEs
    * Overwriting or deleting can take some time to propagate
* Tiered storage classes
    * S3 Standard
        * 4 9s availability, 11 9s durability
        * Designed to sustain the loss of 2 facilities concurrently
    * S3-IA (Infreqeuntly Accessed)
        * Cheaper storage fee than standard, retrieval fee charged
    * S3 One Zone-IA
        * For data storage that does not require the multiple availability zone data resilience 
    * Glacier
        * Very cheap, but used for archival only
        * 3 tiers-- Expedited (minutes), Standard (few hours), Bulk (many hours)
* Key / value store
    * Key-- name
    * Value-- data
    * Version ID
    * Metadata
    * Access control lists
* Versioning allows storage of all versions of an object
    * Includes all writes and deletes
    * Pay per each version
    * Cannot be disabled, only suspended
    * Integrates with lifecycle rules
    * Can require MFA to delete
* Lifecycle management
    * Automatically transition to IA and / or Glacier
        * IA transition minimum is 30 days after creation
        * Glacier transition minimum is 30 days after transition to IA, if applicable
    * Can automatically permanently delete
* Cloudfront
    * Edge locations are where content will be cached
    * Origin is where the files live that the cdn will distribute
        * Bucket
        * EC2 
        * ELB
        * Route53
    * A distribution is the name for a collection of edge locations
        * One distribution can have multiple origins
        * Two types-- web or RTMP
    * Can be READ or WRITE
    * Cached for the life of the TTL
        * By default, objects are cached for 24 hours
    * Can manually clear cached objects, but will be charged
* Bucket security
    * Private by default
    * Bucket policies are applied at the bucket level
    * Access control lists can be applied at the object level
    * Buckets can create access logs
        * Can log to the bucket itself or a different bucket
    * Encryption
        * In transit
            * SSL or TLS
        * At rest
            * Server-side encryption
                * S3 Managed Keys (SSE-S3)
                * AWS Key Management Service (SSE-KMS)
                * Customer-provided Keys (SSE-C)
                * s3 managed keys (sse-s3)
            * Client-side encryption
                * Data is encrypted by the client then uploaded to S3
* Storage gateway
    * File Gateway
        * flat files only
    * Volume Gateway
        * Stored Volumes-- entire dataset stored on-site
        * Cached Volumes-- entire dataset stored in s3, recently-accessed data is stored on-site
    * Virtual Tape Library (VTL)
        * Used for backups
* Snowball
    * Snowball-- pure storage
    * Snowball Edge-- storage with compute
    * Snowmobile-- storage in a shipping container
    * Any of them can import to or export from S3
* Transfer acceleration
    * Speed up transfers to S3, primarily for people far from S3 bucket location
    * Upload files to edge location which then transfers the data to the S3 bucket
* Static Websites
    * Can use S3 to host a static website
    * Cheap and automatic scaling

* HTTP 200 on a successful write
* Upload faster by enabling multipart uploads
    * Breaks large files into multiple pieces

