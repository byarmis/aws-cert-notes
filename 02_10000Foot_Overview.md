# 10,000 Foot Overview

## AWS Global Infrastructure
* 19 regions, 57 availability zones
* Will never be tested on exact numbers, for what it's worth

### Region
* A geographical area (London, Virginia, etc.)
* Consists of 2 or more availability zones

### Availability Zone (AZ)
* A data center in a region
* A disaster in one data center in an AZ should hopefully not affect another 

### Edge Location
* End point for AWS used for caching content
* Typically consists of CloudFront, a Content Delivery Network (CDN)
* More edge locations than regions

## Compute
### EC2
* Elastic Compute Cloud
* Virtual machines in AWS platform

### EC2 Container Service
* Manage Docker containers at scale

### Elastic Beanstalk
* Automatic provisionnig of autoscaling groups, load balancers

### Lambda
* Don't have to worry about machines (physical or virtual)
* Serverless

### Lightsail
* Virtual Private Server (VPS) service
* Server provisioning
* Watered down version of EC2

### Batch
* Batch computing

## Storgage
### S3
* Simple storage service
* Object based storage
* Upload files into buckets

### EFS
* Elastic File System
* Network attached storage

### Glacier
* Data archival
* Cheap and slow

### Snowball
* Data ingestion
* Amazon sends a physical disk to you and then you send it back with the data you want to upload

### Storage Gateway
* Virtual machines installed to replicate data back to AWS

## Databases
### Relational Database Service (RDS)
* MySQL, PostGres, Aurora, Oracle SQL, etc. on AWS

### DynamoDB
* Nonrelational dabtabaases

### Elasticache
* Cache commonly queried things

### Red Shift
* Data warehousing and business intelligence

## Migration
### AWS Migration Hub
* Tracking service
* Track apps as you migrate to AWS

### Application Discovery Service
* Automated application and dependency detection

### Database Migration Service
* Migrate DBs from on-prem to AWS

### Server Migtaion Service
* Migrate phys and virutal servers to AWS

### Snowball
* See above

## Networking and Content Delivery
### Virtual Private Cloud (VPC)
* Virtual data center

### CloudFront
* Content Delivery Network (CDN)
* Physically closer caching to users

### Route53
* DNS service

### API Gateway
* Creating APIs for other services

### Direct Connect
* Run a dedicated line into AWS

## Developer Tools
### CodeStar
* Project managing code
* Set up continuous delivery toolchain

### CodeCommit
* Source control service
* Private git repos

### CodeBuild
* Compiles and tests code

### CodeDeploy
* Automates application deployment to on-prem and EC2 and Lambda

### CodePipeline
* Continueous delivery service

### X-Ray
* Debug and analyze serverless applications

### Cloud9
* Cloud-based IDE
* AWS acquisiiton

## Management Tools
### CloudWatch
* Monitoring service

### CloudFormation
* Scripting infrastructure
* Turn your infrastructure into code

### CloudTrail
* AWS environment modification logging

### CloudConfig
* Point in time snapshot of AWS environ

### OpsWorks
* Automates environments using Chef and Puppet

### Service Catalog
* Managing what's approved

### Systems Manager
* Interface for managing resourcs, primarily EC2
* Patch rollouts

### Trusted Advisor
* Gives advice around security, service utilization, how to save money

### Managed Services
* Helps with EC2 management and autoscaling configurations

## Media Services
### Elastic Transcoder
* Automatic video transcoding

### MediaConvert
* File-based video transcoding

### MediaLive
* Live video processing and streaming

### MediaPackage
* prepares and protects video for delivery

### MediaStore
* Storage service optimized for media

### MediaTailor
* Targeted advertising into video streams

## Machine Learning
### Sagemaker
* Makes it easy to use deep learning

### Comprehend
* Sentiment analysis

### DeepLens
* Artificially aware camera
* On device, can determine what the camera is looking at

### Lex
* What powers alexa
* Artificially intelligent way of chatting with customers

### Machine Learning
* Outcome prediction with datasets

### Polly
* Text-to-speech

### Rekognition
* Video and image
* Upload a file, it tells you what's in the picture or video

### Amazon Translate
* Machine translation

### Amazon Transcribe
* Speech-to-text

## Analytics
### Athena
* Run SQL queries against files in S3 buckets

### Elastic Map Reduce (EMR) 
* Break up data and analyze it

### CloudSearch
* Search service

### ElasticSearch Service
* Search service

### Kinesis
* Real-time data streaming, ingestion, and processing

### Kinesis Video Streams
* Real-time video stream processing

### QuickSight
* AWS' BI tool

### Data Pipeline
* Move data between AWS services

### Glue
* ETL execution and management

## Security, Identity, and Compliance
### Identity Access Management (IAM)
* Users and group permission management

### Cognito
* Device authentication
* Request temporary access to AWS resources

### GuardDuty
* Monitors for malicious activity on AWS accounts

### Inspector
* Agent that's installed on EC2 instances
* Generates vulnerability report

### Macie
* Scan S3 buckets and looks for publically accessible PII

### Certificate Manager
* SSL certificate management

### CloudHSM
* Hardware security module
* Key storage in hardware

### Directory Service
* Integrate Microsoft Active Directory with AWS

### Web Application Firewall (WAF)
* Layer 7 firewall (application layer)
* Stop CSS scripting, SQL injection

### Shield
* DDOS mitigation

### Artifact
* Portal to download AWS compliance reports

## Mobile Services
### Mobile Hub
* Management console

### Pinpoint
* Targeted push notifications to drive mobile engagement

### AppSync
* Automatically update data in web and mobile applications in real time
* Updates data for offline users as soon as they reconnect

### Device Farm
* Test applications on physical devices

### Mobile Analytics
* What it says on the tin

## AR / VR
### Sumerian
* Create and run ar and vr applications 

## Application Integration
### Step functions
* Manage Lambda functions

### Amazon MQ
* Message queues

### Simple Notification Service (SNS)
* Sends texts or emails

### Simple Queue Service (SQS)
* Decoupling infrastructure

### Simple Workflow Service (SWF)
* Build, run, and scale background jobs with parallel and sequential steps

## Customer Enagemnt
### Connect
* Contact center as a service

### Simple Email Service (SES)
* Sending large amounts of email

## Business Productivity
### Alexa for Business
* Alexa for business-related things

### Chime
* Video conferencing

### Work Docs
* Dropbox for AWS

### WorkMail
* Email through Amazon

## Desktop and App Streaming
### Workspaces
* Virutal Desktop Infrastructure (VDI) 
* Run OS in AWS cloud, streamed to device

### AppStream 2.0
* Run applicaiton in cloud, stream to device

## IOT
### IOT

### IOT Device Management

### Amazon FreeRTOS

### Greengrass
* IOT devices running AWS Lambda functions

## Game Development
### GameLift
* Game development service

