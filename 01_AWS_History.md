# AWS History

* 2003
    * Presented a paper on what Amazon's internal infrastructure should look like
    * Suggested selling extra as a service
* 2004
    * Simple Queue Service (SQS) launched
* 2006
    * AWS officially launched
* 2007
    * Over 180,000 developers on the platform
* 2010
    * All of Amazon.com moved over
* 2012
    * First reInvent conference
* 2013
    * First certifications launched
* 2015
    * $6 billion in revenue
* 2016
    * Run rate of $13 billion

