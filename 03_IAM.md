# Identity Access Management (IAM)
## Overview

Allows you to manage users and their level of access.

Gives you: 
* Centralized control of your AWS account
* Shared access
* Granular permissions
* Identity federation (Active Directory, Facebook, LinkedIn)
* Multifactor authentication
* Provide temporary access for users and devices.
* Set up and manage password rotation
* Integrates with a bunch of other AWS services
* Supports PCI DSS Compliance

## Vocabulary
### User
End users / people

### Groups
A colleciton of users with one set of permissions.
Each user in a group will have the permissions of that group.

### Roles
Created and assigned to AWS resources

> For instance, create EC2 instance and give it a role to access S3.  That way, the instance can read and write to and from S3 without having an instance-specific username and password that has to be managed.

### Policies
A JSON document that defines one or more permissions.
These can be attached to users, groups, and roles

## Lab
* Can customize sign-in link with custom DNS
* Access Key IDs and Secret Access Keys are used to programatically interact with AWS
    * You only get access to these once.  If you lose them, you'll have to regenerate them
    * Only used to programatically access AWS-- can't use them to log in

* Delete your root access keys
    * Already ticked
* Activate MFA on your root account
    * Root account gives you unlimited access on an account
    * If someone gets your username and password, they still won't be able to sign in
* Create individual IAM users
    * User Details
        * Pick user name
        * Can have programmatic access and / or web console access
        * Can have an autogenereated or custom default password and optionally require password reset
    * Set permissions
        * Add user to group
            * Can create group from this page
            * Policies can be attached to users or groups
            * AdministratorAccess allows access to do anything on anything
                * Probably don't want to give this sort of access to many people
        * Copy permissions from existing user
        * Attach existing policies directly
        * By default, new users have no permissions when first created
* Use groups to assign permissions
    * Can change users' group membership after the user is created
    * Can change users' permissions separate from their group
    * If a user is inactive, they are not able to programatically access AWS
* Apply an IAM password policy
    * Password policy-- minimum length, expiration time-frames, etc.

* Roles
    * A way to grant permissions to entities that you trust
    * Create a role to allow EC2 instances to write a file to S3
    * If you give admin access to an EC2 instance, you've just given it God Mode.  You probably don't want this

## Create a Billing Alarm Lab
Don't end up accidentally spending too much per month

* Go to My Billing Dashboard -> Alerts & Notifications
* Alarms -> Billing -> Create Alarm
* Select monthly charge limit and email to send alert to
    * Need to verify email before it'll work
* This is part of CloudWatch

